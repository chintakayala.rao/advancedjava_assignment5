package com.nagarro.boot.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="booklist1")
public class BookList {
	@Id
	private int bookcode;
	private String bookName;
	private String author;
	public LocalDate localDate=java.time.LocalDate.now();
	public int getBookcode() {
		return bookcode;
	}
	public void setBookcode(int bookcode) {
		this.bookcode = bookcode;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
	
	public LocalDate getLocalDate() {
		return localDate;
	}
	public void setLocalDate(LocalDate localDate) {
		this.localDate = localDate;
	}
	public BookList(int bookcode, String bookName, String author, LocalDate localDate) {
		super();
		this.bookcode = bookcode;
		this.bookName = bookName;
		this.author = author;
		this.localDate = localDate;
	}
	public BookList() {
		
	}
	@Override
	public String toString() {
		return "BookList [bookcode=" + bookcode + ", bookName=" + bookName + ", author=" + author + ", localDate="
				+ localDate + "]";
	}
	

}
