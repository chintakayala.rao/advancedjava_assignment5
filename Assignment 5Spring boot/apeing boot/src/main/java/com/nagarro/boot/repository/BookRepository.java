package com.nagarro.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nagarro.boot.entity.BookList;


@Repository
public interface BookRepository extends JpaRepository<BookList, Integer> {
 //public void getByBookName();
}
