package com.nagarro.boot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.nagarro.boot.entity.BookList;
import com.nagarro.boot.repository.BookRepository;





@RestController

public class BookController {
	
	 @Autowired
	 BookRepository br;
	/*@Autowired (required=false)
	private BookService bookService;*/

	
	@GetMapping("/book")  
	public List<BookList>getAll()
	{
		//return bookService.getAllBook().toString();
		return br.findAll();
	}
	
	@PutMapping( path="/book")  
	public BookList update(@RequestBody BookList books,@PathVariable  int bookcode)   
	{  
		BookList existingBook= br.getById(bookcode);
		existingBook.setBookcode(bookcode);
		existingBook.setLocalDate(java.time.LocalDate.now());
		existingBook.setAuthor(books.getAuthor());
		existingBook.setBookName(books.getBookName());
		//save updated book
		
		//bookService.UpdateBook(books);
		br.save(existingBook);
	return existingBook;  
	}  
	@PostMapping("/book")  
	public BookList saveBook(@RequestBody BookList books)   
	{  
	//bookService.saveBook(books);
		BookList books2;
		books2=br.save(books);
	return books2;  
	}  
	
	
	@DeleteMapping("/book/{bookcode}")  
	public void deleteBook(@PathVariable("bookcode") int bookcode)   
	{  
		br.deleteById(bookcode);
	//bookService.deleteBookById(bookcode);  
	} 
	@GetMapping("/book/{bookcode}")  
	public Optional<BookList> getBookById (@PathVariable("bookcode") int bookcode)
	{
		Optional<BookList> book=br.findById(bookcode);
		return book;
	}
	
	}  
	
	
	


