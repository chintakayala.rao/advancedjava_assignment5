package com.nagarro.boot.service;

import java.util.List;



import com.nagarro.boot.entity.BookList;


public interface BookService {
	List<BookList> getAllBook();
	   BookList saveBook(BookList bookList);
	   
	   BookList getBookById(int bookcode);
	   
	   BookList UpdateBook(BookList bookList);
	   
	   void deleteBookById(int bookcode);
}
