package com.nagarro.boot.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.boot.entity.BookList;
import com.nagarro.boot.repository.BookRepository;
import com.nagarro.boot.service.BookService;
@Service
public class BookServiceImpl implements BookService {
	 @Autowired
	 private BookRepository bookRepository; 
	@Override
	public void deleteBookById(int bookcode) {
		bookRepository.deleteById(bookcode);
		
	}
	@Override
	public List<BookList> getAllBook() {
		 List<BookList> bookList = bookRepository.findAll();
         
	        if(bookList.size() > 0) {
	            return bookList;
	        } else {
	            return new ArrayList<BookList>();
	        }
	}
	@Override
	public BookList saveBook(BookList bookList) {
		return bookRepository.save(bookList);
	}
	@Override
	public BookList getBookById(int bookcode) {
		return bookRepository.findById(bookcode).get();
	}@Override
	public BookList UpdateBook(BookList bookList) {
		return bookRepository.save(bookList);
	}
	public BookServiceImpl() {
		super();
		this.bookRepository = bookRepository;
		
	}
	

}
