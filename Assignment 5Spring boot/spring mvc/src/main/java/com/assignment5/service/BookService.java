package com.assignment5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment5.entity.BookList;
import com.assignment5.service_integration.BookServiceIngt;






@Service
public class BookService {
	@Autowired
	BookServiceIngt serviceIntegration;
		public  static BookList[] getAllbooks(){
				
				return BookServiceIngt.getBook();
			}
		
		public void updatebook( BookList book)
		{
			serviceIntegration.updateBook(book);
		}
		public  static void saveeBook(BookList book)
		{
			BookServiceIngt.saveBook(book);
		}
		public void deleteBook(int bookcode)
		{
			serviceIntegration.deleteBook(bookcode);
		}
		public BookList getBookById(int bookcode)
		{
			return serviceIntegration.getIdByBook(bookcode);
		}

		public BookService() {
			
		}

}
