package com.assignment5.service_integration;

import java.util.Arrays;
import java.util.HashMap;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.assignment5.entity.BookList;


@Repository
public class BookServiceIngt {
	RestTemplate restTemplate = new RestTemplate();
	
public static BookList[] getBook() {
	RestTemplate restTemplate = new RestTemplate();
	  final String url= "http://localhost:8086/book";
	  HttpHeaders headers = new HttpHeaders();
	  headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	  HttpEntity<String> entity = new HttpEntity<String>("parameters",headers);
	  ResponseEntity<BookList[]> result=restTemplate.exchange(url , HttpMethod.GET, entity, BookList[].class);
	  //List<BookList> books=(List<BookList>)result.getBody();
	  BookList[] books=result.getBody();
	  return books;

		 
	    
		/*List<BookList> bookList=   (List<BookList>) restTemplate.getForObject(url, BookList.class);
	    
	    return bookList;*/
	}
  public static void saveBook(BookList book)
  {
	  RestTemplate restTemplate = new RestTemplate();
	  final String url= "http://localhost:8086/book";
	  HttpHeaders headers = new HttpHeaders();
	  headers.setContentType(MediaType.APPLICATION_JSON);
	  
	  HttpEntity<BookList> entity = new HttpEntity(book,headers);
	  //using post for inert
	  restTemplate.postForEntity(url, entity, BookList.class);
	  //restTemplate.postForObject(url, book, BookList.class);
	  
	
  }
  public void updateBook(BookList book)
  {
	  final String url= "http://localhost:8086/book"+book.getBookcode();
	  Map<String,Integer> param = new HashMap<String,Integer>();
	  param.put("bookcode", book.getBookcode());
	  //BookList bookobj = 
	  //using put for update
	  restTemplate.put(url, book,param);
	//  restTemplate.
	
  }
  public void deleteBook(int bookcode)
  
  {
	  final String url= "http://localhost:8086/book/{bookcode}";
	  
	  Map<String,Integer> param = new HashMap<String,Integer>();
	  param.put("bookcode", bookcode);
	  
	  
	//  delete for deleting
	 restTemplate.delete(url,param);
	
  }
  public BookList getIdByBook(int bookcode)
  {
	  final String url= "http://localhost:8086/book/"+bookcode;
	  Map<String,Integer> param = new HashMap<String,Integer>();
	  param.put("bookcode", bookcode);
	  
	  BookList booklist=restTemplate.getForObject(url,BookList.class,param);
	/*  HttpHeaders headers = new HttpHeaders();
	 
	  headers.setContentType(MediaType.APPLICATION_JSON);
	  HttpEntity<String> entity = new HttpEntity<String>("parameters",headers);
	  ResponseEntity<BookList> result =restTemplate.exchange(url , HttpMethod.GET, entity, BookList.class);
	//  delete for deleting
	  
	  BookList booklist = result.getBody();*/
	  return booklist;
	
  }
  
  
}
