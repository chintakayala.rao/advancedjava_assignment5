package com.assignment5.controller;

import java.io.IOException;
import java.time.LocalDate;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.assignment5.entity.BookList;
import com.assignment5.service.BookService;

// hello



@Controller
@RequestMapping("/")
public class mainController {
	public LocalDate localDate=java.time.LocalDate.now();
	@Autowired
	private  BookService bokService;
	@RequestMapping(path="/home", method = RequestMethod.POST)
	public String homePage(Model m) {
		BookList[] bookList = BookService .getAllbooks();
		System.out.println(bookList);
		m.addAttribute("book",bookList);
		
		return "index.jsp";
	}
	@RequestMapping(path="/home", method = RequestMethod.GET)
	public static String homePage1(Model m) {
		BookList[] bookList = BookService .getAllbooks();
		for(BookList b:bookList)
		System.out.println(b);
		
		m.addAttribute("book",bookList);
		
		return "index.jsp";
	}
	
	@RequestMapping(path="/edit/{bookcode}")
	public String editPage(@PathVariable("bookcode") int bookCode,Model m,HttpServletRequest request,HttpServletResponse response) {
		
		BookList books= bokService.getBookById(bookCode);
		System.out.println(books.getBookcode()+"in first page");
		
		m.addAttribute("books", books.getBookcode());
		return "Edit.jsp";
	}
	
	@RequestMapping(path="/update",method = RequestMethod.POST)
	public String updatePage(
			@ModelAttribute("update") BookList book
			
			) { 
		int id=book.getBookcode();
		System.out.println(id);
		
		//BookList books= bokService.getBookById(bookCode);
		//BookList booklist=new BookList(bookCode, bookName, author,books.localDate );
		
		bokService.updatebook(book);
		
		return "redirect:/index.jsp";
		
	}
	@RequestMapping(path="/addbook",method = RequestMethod.POST)
	public String addPage(@RequestParam("bookcode") int bookCode,	@RequestParam("bookName") String bookName,
			
			@RequestParam("author") String author,HttpServletRequest request,Model m) {
		
		BookList booklist=new BookList(bookCode, bookName, author,localDate );
		
		BookService.saveeBook(booklist);
		
		return "redirect:/index.jsp";
	}
	
	@RequestMapping(path="/delete/{bookcode}")
	public String deletePage(@PathVariable("bookcode") int bookCode) {
		
		System.out.println(bookCode);
		bokService.deleteBook(bookCode);
		return "redirect:/index.jsp";
	}
	@RequestMapping(path="/get/{bookcode}")
	public BookList getbyid(@PathVariable("bookcode") int bookCode)
	{
		BookList book=bokService.getBookById(bookCode);
		return book;
		
	}

}
