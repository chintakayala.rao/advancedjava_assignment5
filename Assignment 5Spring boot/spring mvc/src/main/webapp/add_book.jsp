<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add a Book</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
 integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
 crossorigin="anonymous">
 <style type="text/css">
 h1 {text-align: center;}
 </style>
</head>
<body>
  <h1>Add Book Detail  </h1>
    <form action="<%=request.getContextPath() %>/addbook"   method ="POST">
    <label>Book code</label>
    <input type="text" name="bookcode"  placeholder="enter book code"/><br>
    <label>Book Name</label>
    <input type="text" name="bookName"  placeholder="enter book name"/><br>
    <label>Author</label>
    <input type="text" name="author" placeholder="enter book Auther"/><br>
    
          
    
    <div class="footer-body">
    <button type="submit" class="btn btn-primary"> Add</button></div>
    </form>
</body>
</html>