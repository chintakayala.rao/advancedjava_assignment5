
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><!DOCTYPE html>
<html>
<head>
<title>Home Page</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
 integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
 crossorigin="anonymous">
 <style >
 h1 {text-align: center;}
 p {text-align: right;}
 </style>
 <%@page import="com.assignment5.service.BookService"%>
<%@page import="com.assignment5.entity.BookList"%>
<%@page import="java.util.*"%>
 </head>
<body>
      <h1> Book Listing </h1>
      
         <p>
         <!-- <a href="/Books/add" class="btn btn-primary">Add a Book</a> -->
         <button class="btn btn-primary" onclick="location.href = 'add_book.jsp';">Add a Book</button>  
          </p>
      <div class="container">
           
         <table class="table table-striped table-bordered">
           <thead>
			<tr>
				<th>BookCode</th>
				<th>BookName</th>
				<th>Author</th>
				<th>Date added</th>
				<th colspan="2">Actions</th>
			</tr> 
		</thead>
		<%
		
		BookList[] bookList = BookService.getAllbooks();
		for(BookList b:bookList)
			 {
		%>
		<tbody>
		<tr>
			<td><%=b.getBookcode() %></td>
			<td><%=b.getBookName() %></td>
			<td><%=b.getAuthor() %></td>
			<td><%= b.getLocalDate() %></td>
			
			
			<td><a href="<%=request.getContextPath() %>/edit/<%=b.getBookcode() %>" class="btn btn-primary" > Edit</a></td>
			
			<td ><a href="<%=request.getContextPath() %>/delete/<%=b.getBookcode() %>" class="btn btn-primary" > Delete</a></td>
					
		</tr>
		
		     
		</tbody>
		<%
       }
	
        %>
           
         </table>
       
       </div>
       

</body>
</html>
